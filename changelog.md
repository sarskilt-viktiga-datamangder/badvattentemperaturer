# Changelog 0.9

### Added

- Introduced [`Unit`](http://qudt.org/schema/qudt/Unit) 
- Introduced [`hasPoint`](https://brickschema.org/schema/Brick#hasPoint)


### Fixed


### Changed



### To address 

- Separation between a [`LastKnownValueShape`](https://brickschema.org/schema/Brick#LastKnownValueShape) and a [`AbsoluteHumidityObservation`](https://w3id.org/rec/AbsolutHumidityObservation), which properties should connect the classes or should both be included under a [`Sensor`](https://brickschema.org/schema/Brick#Sensor)

- Air quality instead of Humidity Observation, value, unit, timestamp. 

- hasSubstance on class Sensor 
- for example Air Quality or Energy KPI should be a separate class (sosa:Result or sosa:Observation) 

