rdforms_specs.init({
    language: document.targetLanguage,
    namespaces: {
    },
    bundles: [
      ['../model.json'],
    ],
    main: [
       'temperature-18',
       'temperature-17'
    ],
    supportive: [
      'Sportsfacilities-135',      
    ]
  });
